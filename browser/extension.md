# Extension

## Firefox
 > https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/      

*see [filter.md](filter.md) for various list*         
                  





***
[adguard-adblocker](https://addons.mozilla.org/en-US/firefox/addon/adguard-adblocker/)                        
[clearurls](https://addons.mozilla.org/en-US/firefox/addon/clearurls/)                                          
[contextsearch-web-ext](https://addons.mozilla.org/en-US/firefox/addon/contextsearch-web-ext/)                        
[cookie-autodelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/)                                  
[multi-account-containers](https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/)                            
[infy-scroll](https://addons.mozilla.org/en-US/firefox/addon/infy-scroll/)                             
[libredirect](https://addons.mozilla.org/en-US/firefox/addon/libredirect/)                                
[neat-url](https://addons.mozilla.org/en-US/firefox/addon/neat-url/)                                
[note-taker](https://addons.mozilla.org/en-US/firefox/addon/note-taker/)                                  
[old-reddit-redirect](https://addons.mozilla.org/en-US/firefox/addon/old-reddit-redirect/)                            
[open-link-in-sidebar](https://addons.mozilla.org/en-US/firefox/addon/open-link-in-sidebar/)                                
[redirector](https://addons.mozilla.org/en-US/firefox/addon/redirector/)                            
[save-page-we](https://addons.mozilla.org/en-US/firefox/addon/save-page-we/)                                  
[sidebery](https://addons.mozilla.org/en-US/firefox/addon/sidebery/)                                  
[skip-redirect](https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/)                             
[tab-sidebar-we](https://addons.mozilla.org/en-US/firefox/addon/tab-sidebar-we/)                                
[temporary-containers](https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/)                           
[ublacklist](https://addons.mozilla.org/en-US/firefox/addon/ublacklist/)                             
[ublock-origin](https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/)                        
***


***
https://addons.mozilla.org/en-US/firefox/addon/adguard-adblocker/                              
https://addons.mozilla.org/en-US/firefox/addon/clearurls/                                  
https://addons.mozilla.org/en-US/firefox/addon/contextsearch-web-ext/                           
https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/                                 
https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/                                 
https://addons.mozilla.org/en-US/firefox/addon/infy-scroll/                               
https://addons.mozilla.org/en-US/firefox/addon/libredirect/                              
https://addons.mozilla.org/en-US/firefox/addon/neat-url/                                 
https://addons.mozilla.org/en-US/firefox/addon/note-taker/                                    
https://addons.mozilla.org/en-US/firefox/addon/old-reddit-redirect/                             
https://addons.mozilla.org/en-US/firefox/addon/open-link-in-sidebar/                            
https://addons.mozilla.org/en-US/firefox/addon/redirector/                            
https://addons.mozilla.org/en-US/firefox/addon/save-page-we/                              
https://addons.mozilla.org/en-US/firefox/addon/sidebery/                                    
https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/                               
https://addons.mozilla.org/en-US/firefox/addon/tab-sidebar-we/                                         
https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/                            
https://addons.mozilla.org/en-US/firefox/addon/ublacklist/                                    
https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/                                         
***





***
```
https://addons.mozilla.org/en-US/firefox/addon/adguard-adblocker/
https://addons.mozilla.org/en-US/firefox/addon/clearurls/
https://addons.mozilla.org/en-US/firefox/addon/contextsearch-web-ext/
https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/
https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/
https://addons.mozilla.org/en-US/firefox/addon/infy-scroll/
https://addons.mozilla.org/en-US/firefox/addon/libredirect/
https://addons.mozilla.org/en-US/firefox/addon/neat-url/
https://addons.mozilla.org/en-US/firefox/addon/note-taker/
https://addons.mozilla.org/en-US/firefox/addon/old-reddit-redirect/
https://addons.mozilla.org/en-US/firefox/addon/open-link-in-sidebar/
https://addons.mozilla.org/en-US/firefox/addon/redirector/
https://addons.mozilla.org/en-US/firefox/addon/save-page-we/
https://addons.mozilla.org/en-US/firefox/addon/sidebery/
https://addons.mozilla.org/en-US/firefox/addon/skip-redirect/
https://addons.mozilla.org/en-US/firefox/addon/tab-sidebar-we/
https://addons.mozilla.org/en-US/firefox/addon/temporary-containers/
https://addons.mozilla.org/en-US/firefox/addon/ublacklist/
https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/
```
***



theres others but heres a start for now




