# phyrox // portapps.io          
> portable firefox
>> change 

## ddl              
[ESR](https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58.7z)                           
[REL](https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61.7z)                     
[DEV](https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45.7z)                      

***                


## [Extended](https://github.com/portapps/phyrox-esr-portable/releases)              

> https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58.7z               

 

<details><summary> v91.9.1-58 </summary>                

https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/checksums.txt                  
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/FirefoxESRSetup-91.9.1-win64.exe                      
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58-setup.exe                                                    
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58.7z                                                
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64.exe                                             
https://github.com/portapps/phyrox-esr-portable/archive/refs/tags/91.9.1-58.zip                                              
https://github.com/portapps/phyrox-esr-portable/archive/refs/tags/91.9.1-58.tar.gz                                

</details>

***          

[checksums.txt](https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/checksums.txt)                    
[FirefoxESRSetup-91.9.1-win64.exe](https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/FirefoxESRSetup-91.9.1-win64.exe)         
[phyrox-esr-portable-win64-91.9.1-58-setup.exe](https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58-setup.exe)                             
[phyrox-esr-portable-win64-91.9.1-58.7z](https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58.7z)                           
[phyrox-esr-portable-win64.exe](https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64.exe)                             

***

```
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/checksums.txt
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/FirefoxESRSetup-91.9.1-win64.exe
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58-setup.exe
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64-91.9.1-58.7z
https://github.com/portapps/phyrox-esr-portable/releases/download/91.9.1-58/phyrox-esr-portable-win64.exe
https://github.com/portapps/phyrox-esr-portable/archive/refs/tags/91.9.1-58.zip
https://github.com/portapps/phyrox-esr-portable/archive/refs/tags/91.9.1-58.tar.gz
```

***


## [Default](https://github.com/portapps/phyrox-portable/releases)                
> https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61.7z       

***                   

<details><summary> v100.0.2-61 </summary>                       

https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/checksums.txt                                  
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/FirefoxSetup-100.0.2-win64.exe                              
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61-setup.exe                        
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61.7z                       
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64.exe                       
https://github.com/portapps/phyrox-portable/archive/refs/tags/100.0.2-61.zip                     
https://github.com/portapps/phyrox-portable/archive/refs/tags/100.0.2-61.tar.gz                            

</details>                 

***

[checksums.txt](https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/checksums.txt)                       
[FirefoxSetup-100.0.2-win64.exe](https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/FirefoxSetup-100.0.2-win64.exe)                        
[phyrox-portable-win64-100.0.2-61-setup.exe](https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61-setup.exe)                         
[phyrox-portable-win64-100.0.2-61.7z](https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61.7z)                      
[phyrox-portable-win64.exe](https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64.exe)                        

***

```
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/checksums.txt
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/FirefoxSetup-100.0.2-win64.exe
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61-setup.exe
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64-100.0.2-61.7z
https://github.com/portapps/phyrox-portable/releases/download/100.0.2-61/phyrox-portable-win64.exe
https://github.com/portapps/phyrox-portable/archive/refs/tags/100.0.2-61.zip
https://github.com/portapps/phyrox-portable/archive/refs/tags/100.0.2-61.tar.gz
```




## [Developer](https://github.com/portapps/phyrox-developer-portable/releases)
> https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45.7z        

***


<details><summary> v101.0b9-45 </summary>                             

https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/checksums.txt                                         
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/FirefoxDevEditionSetup-101.0b9-win64.exe                                    
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45-setup.exe                                 
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45.7z                                  
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64.exe           
https://github.com/portapps/phyrox-developer-portable/archive/refs/tags/101.0b9-45.zip                       
https://github.com/portapps/phyrox-developer-portable/archive/refs/tags/101.0b9-45.tar.gz                                  

</details>

***

[checksums.txt](https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/checksums.txt)                  
[FirefoxDevEditionSetup-101.0b9-win64.exe](https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/FirefoxDevEditionSetup-101.0b9-win64.exe)             
[phyrox-developer-portable-win64-101.0b9-45-setup.exe](https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45-setup.exe)                   
[phyrox-developer-portable-win64-101.0b9-45.7z](https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45.7z)                   
[phyrox-developer-portable-win64.exe](https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64.exe)                            



```
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/checksums.txt
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/FirefoxDevEditionSetup-101.0b9-win64.exe
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45-setup.exe
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64-101.0b9-45.7z
https://github.com/portapps/phyrox-developer-portable/releases/download/101.0b9-45/phyrox-developer-portable-win64.exe
https://github.com/portapps/phyrox-developer-portable/archive/refs/tags/101.0b9-45.zip
https://github.com/portapps/phyrox-developer-portable/archive/refs/tags/101.0b9-45.tar.gz
```

                            

