# filter &&|| filterlist        

## filterlist


```
https://abp.oisd.nl/basic/
https://adaway.org/hosts.txt
https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt
https://malware-filter.gitlab.io/malware-filter/urlhaus-filter-agh-online.txt
https://perflyst.github.io/PiHoleBlocklist/AmazonFireTV.txt
https://perflyst.github.io/PiHoleBlocklist/SessionReplay.txt
https://perflyst.github.io/PiHoleBlocklist/SmartTV-AGH.txt
https://perflyst.github.io/PiHoleBlocklist/SmartTV.txt
https://perflyst.github.io/PiHoleBlocklist/android-tracking.txt
https://perflyst.github.io/PiHoleBlocklist/regex.list
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Alternate%20versions%20Anti-Malware%20List/AntiMalwareAdGuardHome.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ClearURLs%20for%20uBo/clear_urls_uboified.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/Dandelion%20Sprout's%20Anti-Malware%20List.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/GameConsoleAdblockList.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/IHateOverpromotedGames.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ImgurPureImageryExperience.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener-AffiliateTagAllowlist.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt
https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/SmartTV-AGH.txt
https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt
https://raw.githubusercontent.com/durablenapkin/scamblocklist/master/adguard.txt
https://raw.githubusercontent.com/hoshsadiq/adblock-nocoin-list/master/hosts.txt
https://raw.githubusercontent.com/mitchellkrogza/The-Big-List-of-Hacked-Malware-Web-Sites/master/hosts
https://raw.githubusercontent.com/ph00lt0/blocklists/master/blocklist.txt
https://someonewhocares.org/hosts/zero/hosts
```



